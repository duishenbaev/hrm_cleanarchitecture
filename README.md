# Hrm_CleanArchitecture

## About project
This HRM system - is a ASP.NET MVC Web application to manage activities of HR-issues administring developed according DDD approach and design patterns as CQRS pattern with MediatR library, Repository pattern and UoW pattern. 

### Features:
 - Create/Review/Update/Delete departments
 - Create/Review/Update/Delete positions
 - Create/Review/Update/Delete employees
---
## Technical stack
 - Web Framework - ASP.NET Core
 - ORM - Entity Framework Core
 - Database - PostgreSQL
 - CQRS - MediatR
 - Testing - xUnit

 ## How to Run

1- Install the following:
* [Microsoft Visual Studio](https://visualstudio.microsoft.com/vs/community/)
* [Docker](https://docs.docker.com/desktop/install/windows-install/)
* [Start a postgres instance](https://hub.docker.com/_/postgres) 

2- Setup the following:
* In `appsettings.json` set fields "DefaultConnection" and "connectionString" as your connection string of PostgreSQL
* Apply migrations with command  
  `dotnet ef database update --project .\Persistance\ --startup-project .\HRM.MVC\`

3- Everything is setup now! You can run the Visual Studio Project by opening HRM.sln and then click run button.