using Domain.Enums;

namespace HRM.MVC.ViewModels;

public class EditEmployeeViewModel
{
    public int Id { get; set; }
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set; } = null!;
    public DateTime BirthDate { get; set; }
    public int PositionId { get; set; }
    public int DepartmentId { get; set; }
    public Gender Gender { get; set; }
}