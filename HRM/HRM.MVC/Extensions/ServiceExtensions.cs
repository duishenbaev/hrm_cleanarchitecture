using Application.CQRS.Queries.Departments;
using Domain.Entities;
using Domain.Interfaces;
using HRM.MVC.Constants;
using Microsoft.EntityFrameworkCore;
using Persistence.DatabaseContext;
using Persistence.RepositoryImplementations;

namespace HRM.MVC.Extensions;

public static class ServiceExtensions
{
    public static IServiceCollection ConfigureServices(this IServiceCollection services)
    {
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        return services;
    }
    
    public static IServiceCollection ConfigureDbContext(this IServiceCollection services,
        WebApplicationBuilder builder)
    {
        var connectionStringName = ServiceExtensionSettings.DefaultConnection;

        const int connectionsRetriesCount = 5;

        services.AddDbContext<HrmContext>(opt =>
        {
            opt.UseNpgsql(builder.Configuration.GetConnectionString(connectionStringName), 
                optionsBuilder =>
            {
                optionsBuilder.EnableRetryOnFailure(connectionsRetriesCount);
            });
        });
        return services;
    }
}