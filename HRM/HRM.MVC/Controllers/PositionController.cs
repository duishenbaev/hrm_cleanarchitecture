using Application.CQRS.Commands.Positions;
using Application.CQRS.Queries.Departments;
using Application.CQRS.Queries.Positions;
using Application.DTOs;
using Domain.Entities;
using HRM.MVC.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace HRM.MVC.Controllers;

public class PositionController : Controller
{
    private readonly IMediator _mediator;

    public PositionController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var query = new GetAllPositionsQuery();
        var positions = await _mediator.Send(query);
        var departmentQuery = new GetAllDepartmentsQuery();
        await _mediator.Send(departmentQuery);
        return View(positions.ToList());
    }
    
    [HttpGet]
    public async Task<IActionResult> Add()
    {
        var query = new GetAllDepartmentsQuery();
        var departments = await _mediator.Send(query);
        ViewBag.departments = new SelectList(departments, "Name", "Name");
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Add(AddPositionViewModel model)
    {
        if (!ModelState.IsValid) return RedirectToAction("Add");
        var departments = await _mediator.Send(new GetAllDepartmentsQuery()) as List<Department>;
        if (departments is null) throw new Exception("Company doesn't have such department!");
        var department = departments.SingleOrDefault(d => d.Name == model.DepartmentName);
        if (department is null) throw new Exception("Such department doesn't exist!");
        var positionDto = new PositionDto(model.Name, department.Id);
        var command = new CreatePositionCommand(positionDto);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public async Task<IActionResult> GetById(int positionId)
    {
        var query = new GetPositionByIdQuery(positionId);
        var response = await _mediator.Send(query);
        return View(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(int positionId)
    {
        var command = new DeletePositionCommand(positionId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(int positionId)
    {
        var query = new GetPositionByIdQuery(positionId);
        var position = await _mediator.Send(query);
        return View(position);
    }
    
    [HttpPost]
    public async Task<IActionResult> Update(IFormCollection collection)
    {
        var positionId = int.Parse(collection["Id"]!);
        var departmentId = int.Parse(collection["departmentId"]!);
        var name = collection["Name"];
        var positionDto = new PositionDto(name!, departmentId);
        var command = new UpdatePositionCommand(positionDto, positionId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }
}