namespace Domain.Entities;

public class Department : BaseEntity
{
    private readonly List<Position> _positions = new();
    public string Name { get; private set; } = null!;
    public IReadOnlyCollection<Position> Positions => _positions;

    public Department(string name) : this()
    {
        Name = name;
    }

    private Department()
    {
    }
}