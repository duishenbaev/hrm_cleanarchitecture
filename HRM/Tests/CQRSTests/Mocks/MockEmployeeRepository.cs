using Domain.Entities;
using Domain.Enums;
using Domain.Interfaces;
using Domain.ValueObjects;
using Moq;

namespace Tests.CQRSTests.Mocks;

public class MockEmployeeRepository
{
    public static Mock<IEmployeeRepository> GetEmployeeRepository()
    {
        var user1 = new UserAccount(
            "Eric", 
            "Cartman", 
            "cartman@mail.net", 
            new DateOnly(2000,01,01));
        var user2 = new UserAccount(
            "Stanley", 
            "Marsh", 
            "marsh@mail.net", 
            new DateOnly(2000,01,01));
       
        var dep = new Department("Back-end .NET") { Id = 1 };
        var pos1 = new Position("Junior SDE", dep) { Id = 1 };
        var pos2 = new Position("Middle SDE", dep) { Id = 2 };

        var empl1 = new Employee(user1, Gender.Male, pos1.Id, pos1) { Id = 1 };
        var empl2 = new Employee(user2, Gender.Male, pos2.Id, pos2) { Id = 2 };

        var employees = new List<Employee>() { empl1, empl2 };
        
        var mockRepo = new Mock<IEmployeeRepository>();
        mockRepo.Setup(x => x.GetAllAsync(It.IsAny<bool>()))
            .ReturnsAsync(employees);
        mockRepo.Setup(x => x.GetByIdAsync(It.IsAny<int>(), It.IsAny<bool>()))
            .Returns((int id, bool value) =>
            {
                var targetEmployee = employees.SingleOrDefault(x => x.Id == id);
                return Task.FromResult(targetEmployee!);
            });
        mockRepo.Setup(x => x.AddAsync(It.IsAny<Employee>()))
            .Returns((Employee employee) =>
            {
                employees.Add(employee);
                return Task.FromResult(employee);
            });
        mockRepo.Setup(x => x.Remove(It.IsAny<Employee>()))
            .Callback((Employee employee) =>
            {
                employees.Remove(employee);
            });
        mockRepo.Setup(x => x.Update(It.IsAny<Employee>()))
            .Callback((Employee employee) =>
            {
                var index = employees.IndexOf(employee);
                employees[index] = new Employee(
                    employee.Account, 
                    employee.Gender, 
                    employee.PositionId, 
                    employee.Position) { Id = employee.Id };
            });
        return mockRepo; 
    }
}