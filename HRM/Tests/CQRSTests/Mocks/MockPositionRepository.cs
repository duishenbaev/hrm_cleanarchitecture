using Domain.Entities;
using Domain.Interfaces;
using Moq;

namespace Tests.CQRSTests.Mocks;

public class MockPositionRepository
{
    public static Mock<IPositionRepository> GetPositionRepository()
    {
        var dep = new Department("Back-end .NET") { Id = 1 };
        var pos1 = new Position("Junior SDE", dep) { Id = 1 };
        var pos2 = new Position("Middle SDE", dep) { Id = 2 };

        var positions = new List<Position> { pos1, pos2 };

        var mockRepo = new Mock<IPositionRepository>();
        mockRepo.Setup(x => x.GetAllAsync(It.IsAny<bool>()))
            .ReturnsAsync(positions);
        mockRepo.Setup(x => x.GetByIdAsync(It.IsAny<int>(), It.IsAny<bool>()))
            .Returns((int id, bool value) =>
            {
                var targetPosition = positions.SingleOrDefault(x => x.Id == id);
                return Task.FromResult(targetPosition!);
            });
        mockRepo.Setup(x => x.AddAsync(It.IsAny<Position>()))
            .Returns((Position position) =>
            {
                positions.Add(position);
                return Task.FromResult(position);
            });
        mockRepo.Setup(x => x.Remove(It.IsAny<Position>()))
            .Callback((Position position) =>
            {
                positions.Remove(position);
            });
        mockRepo.Setup(x => x.Update(It.IsAny<Position>()))
            .Callback((Position position) =>
            {
                var index = positions.IndexOf(position);
                positions[index] = new Position(position.Name, position.Department) { Id = position.Id };
            });
        return mockRepo; 
    }
}