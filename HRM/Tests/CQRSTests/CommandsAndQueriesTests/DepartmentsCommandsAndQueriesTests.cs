using Application.CQRS.Commands.Departments;
using Application.CQRS.Queries.Departments;
using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using Moq;
using Shouldly;
using Tests.CQRSTests.Mocks;
using Xunit;

namespace Tests.CQRSTests.CommandsAndQueriesTests;

public class DepartmentsCommandsAndQueriesTests
{
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;

    public DepartmentsCommandsAndQueriesTests()
    {
        _mockUnitOfWork = MockUnitOfWork.GetMockUnitOfWork();
    }
    
    [Fact]
    public async Task GetAllDepartmentsQuery_CheckAllCurrentDepartmentsNumber_ShouldBeTwo()
    {
        //Arrange
        var query = new GetAllDepartmentsQuery();
        var handler = new GetAllDepartmentsQuery.GetAllDepartmentsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Count().ShouldBe(2);
    }
    
    [Fact]
    public async Task GetAllDepartmentsQuery_CheckTypeOfCollectionsElements_ShouldBeListOfDepartments()
    {
        //Arrange
        var query = new GetAllDepartmentsQuery();
        var handler = new GetAllDepartmentsQuery.GetAllDepartmentsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<List<Department>>();
    }
    
    [Fact]
    public async Task GetAllDepartmentsQuery_CheckResultOfQueryToNull_ShouldNotBeNull()
    {
        //Arrange
        var query = new GetAllDepartmentsQuery();
        var handler = new GetAllDepartmentsQuery.GetAllDepartmentsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task GetDepartmentByIdQuery_CheckTypeOfQueriesResult_ShouldBeDepartment()
    {
        //Arrange
        const int departmentId = 1;
        var query = new GetDepartmentByIdQuery(departmentId);
        var handler = new GetDepartmentByIdQuery.GetDepartmentByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Department>();
    }
    
    [Fact]
    public async Task GetDepartmentByIdQuery_CheckCorrectNameOfQueryResult_ShouldBeBackendDotNet()
    {
        //Arrange
        const int departmentId = 1;
        var query = new GetDepartmentByIdQuery(departmentId);
        var handler = new GetDepartmentByIdQuery.GetDepartmentByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Name.ShouldBe("Back-end .NET");
    }
    
    [Fact]
    public async Task GetDepartmentByIdQuery_CheckQueryResultToNull_ShouldNotBeNull()
    {
        //Arrange
        const int departmentId = 1;
        var query = new GetDepartmentByIdQuery(departmentId);
        var handler = new GetDepartmentByIdQuery.GetDepartmentByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task CreateDepartmentCommand_CheckTypeOfCommandsResult_ShouldBeDepartment()
    {
        //Arrange
        var dto = new DepartmentDto("Back-end Python");
        var query = new CreateDepartmentCommand(dto);
        var handler = new CreateDepartmentCommand.CreateDepartmentCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Department>();
    }
    
    [Fact]
    public async Task CreateDepartmentCommand_CheckCommandsResultToNull_ShouldNotBeNull()
    {
        //Arrange
        var dto = new DepartmentDto("Back-end Python");
        var query = new CreateDepartmentCommand(dto);
        var handler = new CreateDepartmentCommand.CreateDepartmentCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }

    [Fact]
    public async Task CreateDepartmentCommand_CheckCountOfDepartmentsListAfterCreationDepartment_ShouldBeThree()
    {
        //Arrange
        var dto = new DepartmentDto("Back-end Python");
        var query = new CreateDepartmentCommand(dto);
        var handler = new CreateDepartmentCommand.CreateDepartmentCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(query, CancellationToken.None);
        var departmentslist = await _mockUnitOfWork.Object.DepartmentRepository.GetAllAsync(false);
        //Assert
        departmentslist.Count.ShouldBe(3);
    }
    
    [Fact]
    public async Task DeleteDepartmentCommand_CheckNumberOfDepartmentsList_ShouldBeOne()
    {
        //Arrange
        const int departmentId = 1;
        const int expectedDepartmentsCount = 1;
        var handlerDeleteDepartment = new DeleteDepartmentCommand.DeleteDepartmentCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handlerDeleteDepartment.Handle(
            new DeleteDepartmentCommand(departmentId),
            CancellationToken.None);
        var departmentsList = await _mockUnitOfWork.Object.DepartmentRepository.GetAllAsync();
        //Assert
        departmentsList.Count.ShouldBe(expectedDepartmentsCount);
    }
    
    [Fact]
    public async Task UpdateDepartmentCommand_CheckNamesOfObjectBeforeAndAfterUpdate_ShouldBePython()
    {
        //Arrange
        var dto = new DepartmentDto("Back-end Python");
        const int departmentId = 1;
        var request = new UpdateDepartmentCommand(dto, departmentId);
        var handler = new UpdateDepartmentCommand.UpdateDepartmentCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(request, CancellationToken.None);
        var department = await _mockUnitOfWork.Object.DepartmentRepository.GetByIdAsync(departmentId);
        //Assert
        department.Name.ShouldBe("Back-end Python");
    }
}