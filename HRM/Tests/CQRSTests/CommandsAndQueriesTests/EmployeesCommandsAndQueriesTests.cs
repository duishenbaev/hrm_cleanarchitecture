using Application.CQRS.Commands.Employees;
using Application.CQRS.Queries.Employees;
using Application.DTOs;
using Domain.Entities;
using Domain.Enums;
using Domain.Interfaces;
using Domain.ValueObjects;
using Moq;
using Shouldly;
using Tests.CQRSTests.Mocks;
using Xunit;

namespace Tests.CQRSTests.CommandsAndQueriesTests;

public class EmployeesCommandsAndQueriesTests
{
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    
    public EmployeesCommandsAndQueriesTests()
    {
        _mockUnitOfWork = MockUnitOfWork.GetMockUnitOfWork();
    }
    
    [Fact]
    public async Task GetAllEmployeesQuery_CheckAllCurrentEmployeesNumber_ShouldBeTwo()
    {
        //Arrange
        var query = new GetAllEmployeesQuery();
        var handler = new GetAllEmployeesQuery.GetAllEmployeesQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Count().ShouldBe(2);
    }

    [Fact]
    public async Task GetAllEmployeesQuery_CheckTypeOfCollectionsElements_ShouldBeListOfEmployees()
    {
        //Arrange
        var query = new GetAllEmployeesQuery();
        var handler = new GetAllEmployeesQuery.GetAllEmployeesQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<List<Employee>>();
    }
    
    [Fact]
    public async Task GetAllPositionsQuery_CheckResultOfQueryToNull_ShouldNotBeNull()
    {
        //Arrange
        var query = new GetAllEmployeesQuery();
        var handler = new GetAllEmployeesQuery.GetAllEmployeesQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task GetEmployeeByIdQuery_CheckTypeOfQueriesResult_ShouldBeEmployee()
    {
        //Arrange
        const int employeeId = 1;
        var query = new GetEmployeeByIdQuery(employeeId);
        var handler = new GetEmployeeByIdQuery.GetEmployeeByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Employee>();
    }
    
    [Fact]
    public async Task GetEmployeeByIdQuery_CheckCorrectNameOfEmployee_ShouldBeEric()
    {
        //Arrange
        const int employeeId = 1;
        var query = new GetEmployeeByIdQuery(employeeId);
        var handler = new GetEmployeeByIdQuery.GetEmployeeByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Account.Name.ShouldBe("Eric");
    }
    
    [Fact]
    public async Task GetEmployeeByIdQuery_CheckQueryResultToNull_ShouldNotBeNull()
    {
        //Arrange
        const int employeeId = 1;
        var query = new GetEmployeeByIdQuery(employeeId);
        var handler = new GetEmployeeByIdQuery.GetEmployeeByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task CreateEmployeeCommand_CheckTypeOfCommandsResult_ShouldBeEmployee()
    {
        //Arrange
        var user = new UserAccount(
            "Kyle", 
            "Broflovski", 
            "broflovski@mail.net", 
            new DateOnly(2000,01,01));
        const int positionId = 1;
        var dto = new EmployeeDto(user, Gender.Male, positionId);
        var query = new CreateEmployeeCommand(dto);
        var handler = new CreateEmployeeCommand.CreateEmployeeCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Employee>();
    }
    
    [Fact]
    public async Task CreateEmployeeCommand_CheckCommandsResultToNull_ShouldNotBeNull()
    {
        //Arrange
        var user = new UserAccount(
            "Kyle", 
            "Broflovski", 
            "broflovski@mail.net", 
            new DateOnly(2000,01,01));
        const int positionId = 1;
        var dto = new EmployeeDto(user, Gender.Male, positionId);
        var query = new CreateEmployeeCommand(dto);
        var handler = new CreateEmployeeCommand.CreateEmployeeCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task CreateEmployeeCommand_CheckCountOfEmployeesListAfterCreationEmployee_ShouldBeThree()
    {
        //Arrange
        var user = new UserAccount(
            "Kyle", 
            "Broflovski", 
            "broflovski@mail.net", 
            new DateOnly(2000,01,01));
        const int positionId = 1;
        var dto = new EmployeeDto(user, Gender.Male, positionId);
        var query = new CreateEmployeeCommand(dto);
        var handler = new CreateEmployeeCommand.CreateEmployeeCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(query, CancellationToken.None);
        var employeeslist = await _mockUnitOfWork.Object.EmployeeRepository.GetAllAsync(false);
        //Assert
        employeeslist.Count.ShouldBe(3);
    }
    
    [Fact]
    public async Task DeleteEmployeeCommand_CheckNumberOfEmployeesList_ShouldBeOne()
    {
        //Arrange
        const int employeeId = 1;
        const int expectedEmployeesCount = 1;
        var handlerDeleteEmployee = new DeleteEmployeeCommand.DeleteEmployeeCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handlerDeleteEmployee.Handle(
            new DeleteEmployeeCommand(employeeId),
            CancellationToken.None);
        var employeesList = await _mockUnitOfWork.Object.EmployeeRepository.GetAllAsync();
        //Assert
        employeesList.Count.ShouldBe(expectedEmployeesCount);
    }
    
    [Fact]
    public async Task UpdateEmployeeCommand_CheckNamesOfEmployeeBeforeAndAfterUpdate_ShouldBeKyle()
    {
        //Arrange
        //Arrange
        var user = new UserAccount(
            "Kyle", 
            "Broflovski", 
            "broflovski@mail.net", 
            new DateOnly(2000,01,01));
        const int positionId = 1;
        const int employeeId = 1;
        var dto = new EmployeeDto(user, Gender.Male, positionId);
        var request = new UpdateEmployeeCommand(dto, employeeId);
        var handler = new UpdateEmployeeCommand.UpdateEmployeeCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(request, CancellationToken.None);
        var employee = await _mockUnitOfWork.Object.EmployeeRepository.GetByIdAsync(employeeId);
        //Assert
        employee.Account.Name.ShouldBe("Kyle");
    }
}