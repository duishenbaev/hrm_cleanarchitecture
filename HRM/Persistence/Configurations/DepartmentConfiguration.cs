using System.Collections.Immutable;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
{
    public void Configure(EntityTypeBuilder<Department> builder)
    {
        builder.HasKey(e => e.Id);
        builder.Property(e => e.Id).ValueGeneratedOnAdd();
        builder.HasMany(e => e.Positions).WithOne(x => x.Department);
        builder.Property(e => e.Name).HasMaxLength(ConfigurationConstants.NameMaxLength).IsRequired();
    }
}
