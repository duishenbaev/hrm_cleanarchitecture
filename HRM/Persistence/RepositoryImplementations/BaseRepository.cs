using System.Linq.Expressions;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Persistence.DatabaseContext;

namespace Persistence.RepositoryImplementations;

public abstract class BaseRepository<TEntity> : IRepository<TEntity>
    where TEntity : BaseEntity
{
    private readonly HrmContext _context;

    protected BaseRepository(HrmContext context)
    {
        _context = context;
    }
    
    public IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression,
        bool trackChanges = false)
    {
        var result = _context.Set<TEntity>()
            .Where(expression);
        if (!trackChanges) result.AsNoTracking();
        return result;
    }
    
    public async Task AddAsync(TEntity entity)
    {
        await _context.Set<TEntity>().AddAsync(entity);
    }

    public async Task<TEntity> GetByIdAsync(int id, bool trackChangesAsync = false)
    {
        var department = await FindByCondition(e => e.Id == id, trackChangesAsync)
            .SingleOrDefaultAsync();
        if (department == null)
        {
            throw new NullReferenceException("entity is not found in database!");
        }
        return department;
    }

    public async Task<List<TEntity>> GetAllAsync(bool trackChangesAsync = false)
    {
        var result = _context.Set<TEntity>();
        if (!trackChangesAsync) result.AsNoTracking();
        return await result.ToListAsync();
    }

    public void Remove(TEntity entity)
    {
        _context.Set<TEntity>().Remove(entity);
    }

    public void Update(TEntity entity)
    {
        _context.Set<TEntity>().Update(entity);
    }

    public async Task<bool> IsExistsAsync(int id)
    {
        return await _context.Set<TEntity>().AnyAsync(e => e.Id == id);
    }
}