using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Departments;

public class GetAllDepartmentsQuery : IRequest<IEnumerable<Department>>
{
    public class GetAllDepartmentsQueryHandler : IRequestHandler<GetAllDepartmentsQuery, IEnumerable<Department>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetAllDepartmentsQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Department>> Handle(GetAllDepartmentsQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.DepartmentRepository.GetAllAsync();
        }
    }
}