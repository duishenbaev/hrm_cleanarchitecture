using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Positions;

public class DeletePositionCommand : IRequest
{
    private readonly int _positionId;

    public DeletePositionCommand(int positionId)
    {
        _positionId = positionId;
    }

    public class DeletePositionCommandHandler : IRequestHandler<DeletePositionCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeletePositionCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeletePositionCommand request, CancellationToken cancellationToken)
        {
            var position = await _unitOfWork.PositionRepository.GetByIdAsync(request._positionId);
            _unitOfWork.PositionRepository.Remove(position);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}