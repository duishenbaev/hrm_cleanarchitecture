namespace Application.DTOs;

public record PositionDto(string Name, int DepartmentId);